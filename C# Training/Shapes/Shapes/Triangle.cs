﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Triangle : Shapes
    {
        private double length, breadth;
        private double Area;

        public Triangle(double length, double breadth, string name) : base(name)
        {
            this.length = length;
            this.breadth = breadth;
        }

        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() +","+ "Length: " +length + "," +  "Breadth: " + breadth;
        }

        public override string CalArea()
        {
            //Console.WriteLine("Area of Rectangle:");
            Area = 0.5*length*breadth;
            return "Area of Rectangle:" + Area;
        }
    }
}
