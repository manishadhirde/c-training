﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public abstract class Shapes
    {
        string name = String.Empty;
        
        public Shapes(string name)
        {
            this.name = name;
        }

        public virtual string GetShapeInfo()
        {
            return "Name Of Shape: " + name;
        }

        public abstract string CalArea();

    }
}
