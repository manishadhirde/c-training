﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
   public class MainMethod
    {
        private static Shapes[] shape;

        static MainMethod()
        {
            shape = new Shapes[4];
        }

        public static void AddTheObject()
        {
            shape[0] = new Circle("Circle", 4);
            shape[1] = new Rectangle("Rectangle",4, 4);
            shape[2] = new Square("Square", 4);
            shape[3] = new Triangle(4,4,"Triangle");
        }

        public static List<Shapes> UsingList()
        {
            List<Shapes> list = new List<Shapes>();
            list.Add(new Circle("Circle", 4));
            list.Add(new Rectangle("Rectangle", 4, 4));
            list.Add(new Square("Square", 4));
            list.Add(new Triangle(4, 4, "Triangle"));

            return list;
        }

        public static Shapes[] GetShapesObj()
        {
            return shape;
        }
        public static void Main1(string[] args)
        {
            MainMethod.AddTheObject();
           
            Shapes[] objShapes = MainMethod.GetShapesObj();

            foreach (Shapes obj in objShapes)
            {
                Console.WriteLine(obj.GetShapeInfo());
                Console.WriteLine(obj.CalArea());
            }
            
            Console.ReadLine();
        }

        //main method for list obj
        public static void Main(string[] args)
        {
           List<Shapes> objShapes = MainMethod.UsingList();

            //Shapes[] objShapes = MainMethod.GetShapesObj();

            foreach (Shapes obj in objShapes)
            {
                Console.WriteLine(obj.GetShapeInfo());
                Console.WriteLine(obj.CalArea());
            }

            Console.ReadLine();
        }
    }
}
