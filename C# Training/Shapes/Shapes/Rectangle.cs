﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Rectangle : Shapes
    {
        private double length;
        private double breadth;
        double area;

        public Rectangle(string name, double length, double breadth) : base(name)
        {
            this.length = length;
            this.breadth = breadth;
        }

        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + "," + "Length: " + length + "," + "Breadth: " + breadth;
        }

        public override string CalArea()
        {
            //Console.WriteLine("Area of Rectangle: ");
            area = length * breadth;
            return "Area of Rectangle: " + area;
            
        }
    }
}
