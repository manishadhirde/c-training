﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Circle : Shapes
    {
        private static double pi = 3.14;
        private double radious;
        private double Area;

        public Circle(string name, double radious) : base(name)
        {
            this.radious = radious;
           
        }

        public override string GetShapeInfo()
        {
           return base.GetShapeInfo() + "," + "Radious Value: " + radious;
        }

        public override string CalArea()
        {
            //Console.WriteLine("Area of Square: ");
            Area = 3.14*radious*radious;
            return "Area of Square: " + Area;
        }
    }
}
