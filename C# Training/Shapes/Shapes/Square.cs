﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Square : Shapes
    {
        private double side;
        private double Area;

        public Square(string name, double side) : base(name)
        {
            this.side = side;
        }

        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + "," + "Side Of Square: " + side;
        }

        public override string CalArea()
        {
            //Console.WriteLine("Area of Square: ");
            Area = side*side;
            return "Area of Square: " + Area;
        }
    }
}
