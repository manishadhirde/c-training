﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceExample
{
   public class Program : IInterface1, Interface2
    {
         void IInterface1.Addition()
        {
            Console.WriteLine("Addition is Performed by first interface..");
        }

        void Interface2.Addition()
        {
            Console.WriteLine("Addition is Performed by second interface..");
        }
        public void Substraction()
        {
            Console.WriteLine("subtraction is Performed by first interface..");
        }

        public void Addition()
        {
            Console.WriteLine("Addition is Performed by program..");
        }
        static void Main(string[] args)
        {
            Program pre = new Program();
            pre.Substraction();
            IInterface1 interface1  = new Program();
            interface1.Addition();

            Interface2 interface2 = new Program();
            interface2.Addition();

            //IInterface1 inte;
            //pre = interface1;
            Console.ReadKey();


        }
    }
}
