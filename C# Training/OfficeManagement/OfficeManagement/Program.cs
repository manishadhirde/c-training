﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
   public class Program
    {
        static void Main3(string[] args)
        {
            EmpList.AddEmployee();
            Employee[] myEmp = EmpList.GetEmployee();
            //for (int i = 0; i < myEmp.Length; i++)
            //{
            //    Console.WriteLine(myEmp[i].GetEmpInfo());
            //}
            foreach (Employee e in myEmp)
            {
                Console.WriteLine(e.GetEmpInfo());
            }
            Console.ReadKey();
        }
        // Inheritance Main function

        static void Main4(string[] args)
        {
            SalesPerson s = new SalesPerson(101,"komal",290000,89,678);
            Console.WriteLine(s.GetEmpInfo());
            Console.WriteLine(s.CalSal());

            Engineer e = new Engineer(102,"Mohini",298999,78,90);
            Console.WriteLine(e.GetEmpInfo());
            Console.WriteLine(s.CalSal());

            Management m = new Management(103,2345,"Yogita","Engineer",67890);
            Console.WriteLine(s.CalSal());
            Console.WriteLine(m.GetEmpInfo());

            Employee emp = new SalesPerson(104,"Manisha",400000,89,90);
            Console.WriteLine(emp.GetEmpInfo());
            Console.ReadKey();


        }

        //Polymorphism
        private static void Main(string[] args)
        {
            Employee[] emp = new Employee[3];
            emp[0] = new Employee(104, "Manisha", 400000);
            emp[1] = new Employee(103, "Yogita", 2345 );
            emp[2] = new Employee(101, "komal", 290000);

            foreach (Employee e in emp)
            {
               Console.WriteLine(e.GetEmpInfo());
                Console.WriteLine(e.CalSal());
            }
            Console.ReadKey();
        }

        static void Main2(string[] args)
        {
            Employee e = new Employee();
            Console.WriteLine(e.GetEmpInfo());

            Employee e2 = new Employee();
            e2.SetEmpInfo(40010,"Shewta", 230000);
            Console.WriteLine(e2.GetEmpInfo());

            Employee e3 = new Employee(40022,"Harshad",35768);
            //Employee.SetCompanyName("Tricentis");
            Console.WriteLine(e3.GetEmpInfo());

            Employee e4 = new Employee(e3);
            Console.WriteLine(e4.GetEmpInfo());

            Employee e5 = new Employee("Ravi",400,30000000);
            Console.WriteLine(e5.GetEmpInfo());
            Console.WriteLine("Count of Employee =" +Employee.GetCount());
            Console.ReadKey();
        }

        static void Main1(string[] args)
        {
            MyUtilClass m1 = new MyUtilClass();
            Console.Write("Addition Of Two No:");
            Console.WriteLine(m1.Add(12, 13));
            Console.Write("Addition Of Three No:");
            Console.WriteLine(m1.Add(12, 13, 14)); 
            int[] arrr = {1, 2, 3, 4, 5};
            Console.Write("Addition Of Array Elemenets:");
            Console.WriteLine(m1.Add(arrr));
            Console.Write("Addition Of Two String:");
            Console.WriteLine(m1.Add("Manisha", "Dhirde"));

            Console.ReadKey();

        }
    }
}
