﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
   public class MyUtilClass
    {
        public int Add(int Num1, int Num2)
        {
            return Num1 + Num2;
        }

        public int Add(int Num3, int Num4, int Num5)
        {
            return Num3 + Num4 + Num5;
        }

        public string Add(String str1, string str2)
        {
            return str1+" " + str2;
        }

        public int Add(int[] arr)
        {
            return arr.Sum();
        }
    }
}
