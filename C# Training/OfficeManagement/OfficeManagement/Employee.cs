﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    public class Employee
    {
        private int id;
        public string name;
        protected double salary;
        public static  string companyName;

        private static int count;
        //Static Constructor
        static Employee()
        {
            Console.WriteLine("Static Constructor Call");
            count = 100;
            companyName = "Tricentis India";
        }
        //Default Constructor
        public Employee()
        {
            this.id = 40021;
            this.name = "Pranita";
            this.salary = 100000;
            count++;
        }
        //Parameterize constructor
        public Employee(int ID, string Name, double Salary)
        {
            id = ID;
            name = Name;
            salary = Salary;
            count++;
        }
        // Copy Constructor
        public Employee(Employee e)
        {
            this.id = e.id;
            this.name = e.name;
            this.salary = e.salary;
            count++;
        }

        public Employee(string Name, int Id, double sal)
        {
            this.name = Name;
            this.id = Id;
            this.salary = sal;
        }

        public static void SetCompanyName(string company)
        {
             companyName = company ;
        }
        public static string GetCompanyName(string companyName)
        {
            return companyName;
        }
        public void SetEmpInfo(int id, string nm, double salary)
        {
            this.id = id;//this is point the current object, if ur class variable are same as the passing the param for fun. then used 'this' keyword. 
            this.name = nm;
            this.salary = salary;

        }

        public virtual double CalSal()
        {
            return this.salary;
        }

        public void GiveInc(double inc)
        {
            this.salary += inc;
        }

        public static int GetCount()
        {
            return count;
        }
        public virtual string  GetEmpInfo()
        {
            return "Emp Information" + "- " + this.id + ", " + this.name + ", " + this.salary + ", " + companyName;
        }
    }
}
