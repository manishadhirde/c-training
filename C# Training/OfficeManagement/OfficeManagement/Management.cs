﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
   public class Management : Employee
    {
        private string dept;
        private double bonus;

        public Management(int id, double sal,string nm, string dept,double bonus):base(id,nm,sal)
        {
            this.bonus = bonus;
            this.dept = dept;
        }

        //public new string GetEmpInfo()
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + ", " + this.bonus + this.dept;
        }

        //public new double CalSal()
         public override double CalSal()
        {
            return base.salary + this.bonus ;
        }
    }
}
