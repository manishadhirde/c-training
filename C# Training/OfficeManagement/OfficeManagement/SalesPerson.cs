﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    public class SalesPerson : Employee
    {
        private int sales;
        private double commission;
        //the parameter nm of both the class is simillar
        public SalesPerson(int id, string name, double salary, int sales, double commission ) : base(id,name,salary)
        {
            this.commission = commission;
            this.sales = sales;
        }

        //public new string GetEmpInfo()
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + ", " + this.sales + this.commission;
        }

        public override double CalSal()
        {
            return base.salary + (this.commission*this.sales);
        }
    }
}
