﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
   public class Engineer : Employee
    {
        private double extraHrs;
        private double incen;

        public Engineer(int id, string name, double salary,double extraHrs,double incen):base()
        {
            this.extraHrs = extraHrs;
            this.incen = incen;
        }
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + ", " + this.extraHrs + this.incen;
        }

        //public new double CalSal()
        public override double CalSal()
        {
            return base.CalSal() + this.extraHrs + this.incen;
        }
    }
}
