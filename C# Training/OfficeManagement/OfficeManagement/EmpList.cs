﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
  public static class EmpList
    {
        static Employee[] elist;

        static EmpList()
        {
            elist = new Employee[3];
        }
        public static void AddEmployee()
        {
            elist[0] = new Employee(101,"Manisha",200000);
            elist[1] = new Employee(102, "Komal", 290000);
            elist[2] = new Employee(103, "Yogita", 240000);

        }
        public static Employee[] GetEmployee()
        {
            return elist;
        }
    }
}
