﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeManagement;

namespace WindowFormForOfficeMgmt
{
    public partial class Form1 : Form
    { 
         List<OfficeManagement.Employee> elist;
       // private FileStream fs;
        public Form1()
        {
            InitializeComponent();
            DisableTheGroupBoxes();
            elist = new List<Employee>();
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DisableTheGroupBoxes();
            if (comboBox1.SelectedItem.Equals("Manager"))
            {
                elist.Add(new Management(Convert.ToInt32(textBox1.Text),Convert.ToDouble(textBox3.Text), textBox2.Text,textBox7.Text,Convert.ToDouble(textBox6.Text)));
            }
            else if (comboBox1.SelectedItem.Equals("Engineer"))
            {
                elist.Add(new Engineer(Convert.ToInt32(textBox1.Text), textBox2.Text, Convert.ToDouble(textBox3.Text), Convert.ToDouble(textBox4.Text), Convert.ToDouble(textBox5.Text)));
            }
            else
            {
                elist.Add(new SalesPerson(Convert.ToInt32(textBox1.Text),  textBox2.Text, Convert.ToDouble(textBox3.Text),Convert.ToInt32(textBox9.Text), Convert.ToDouble(textBox8.Text)));
            }
            MessageBox.Show("Employee Added..");
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void DisableTheGroupBoxes()
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableTheGroupBoxes();
            if (comboBox1.SelectedItem.Equals("Manager"))
            {
                groupBox2.Enabled = true;
            }
            else if (comboBox1.SelectedItem.Equals("Engineer"))
            {
                groupBox1.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = true;
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            FileStream fs = null;
            if (textBox10.Text.Equals(null))
            {
                fs = new FileStream(textBox10.Text, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            }
            else
            {
                fs = new FileStream(@"C: \Users\mandh\Desktop\FileForEmployee\Employee.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            }
            
            StreamWriter sw = new StreamWriter(fs);

            //for (int i = 0; i < 15; i++)
            //{
            //    sw.WriteLine("Dreamer can be winner, if they belive in their dream");
            //}

            foreach (Employee emp in elist)
            {
                sw.WriteLine(emp.GetEmpInfo() + " Total Sal: " + emp.CalSal());
            }
            sw.Close();
            fs.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                 fs = new FileStream(@"C:\Users\mandh\Desktop\FileForEmployee\Employee.txt", FileMode.Open,
                    FileAccess.ReadWrite, FileShare.ReadWrite);
                 sr = new StreamReader(fs);
                Office.Items.Clear();
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    Office.Items.Add(line);
                }
                //Office.Items.Clear();
                //foreach (Employee emp in elist)
                //{
                //    Office.Items.Add(emp.GetEmpInfo() + " Total Sal: " + emp.CalSal());
                //}
                Console.WriteLine("File Read Successfully !!!");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }

            finally
            {
                sr.Close();
                fs.Close();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Office.Items.Clear();
            foreach (Employee emp in elist)
            {
                Office.Items.Add(emp.GetEmpInfo() + " Total Sal: " + emp.CalSal());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox10.Text = openFileDialog1.FileName;
            }
        }
    }
}
